
variable "region"{
    type = string
}
variable "vpc" {
  type = string
}
variable "ami" {
  type = string
}
variable "itype" {
  type = string
}
variable "publicip" {
  type = bool
}
variable "keyname" {
  type = string
}
variable "secgroupname" {
  type = string
}
variable "ssh_keyname" {
  type = string
}
variable "publickey" {
  type = string
}